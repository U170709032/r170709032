import java.io.File;
import java.io.FileNotFoundException;
import java.util.Random;
import java.util.Scanner;

public class FindOccurrences{
	
	public static void main(String[] args) {
		int [] matrix = readMatrix();
		for(int i=0; i<matrix.length; i++){
			System.out.println(i  +   " occurs  "  +  matrix[i]  + " time(s) " ); 
		}


	}	
	private static int[] readMatrix(){
		int[][] matrix = new int [10][10];
		int[] occurance = new int [10];
		
		File file = new File("matrix.txt");

	    try {

	        Scanner sc = new Scanner(file);
	        int i = 0;
	        int j = 0;
	        while (sc.hasNextLine()) {
	            int number = sc.nextInt();
	            matrix[i][j] = number;	
	            if (j == 9)
	            	i++;
	            j = (j + 1) % 10;
		    for(int a=0; a<matrix.length; a++){
			if(a==number){
			occurance[a] += 1;
			}
		    }
	            if (i==10)
	            	break;
	        }
	        sc.close();
	    } 
	    catch (FileNotFoundException e) {
	        e.printStackTrace();
	    }		
		return occurance;
	}
	
	
}